import numpy as np
import cv2
import threading
from Projector import Fringes

class ProjectorSystem(object):

    def __init__(self, ncam=0, fringes=Fringes()):
        self._frame_list = []
        self._capturer = cv2.VideoCapture(ncam)
        self._ncam = ncam
        self._fringes = fringes
        self._close = False
        self._th = None
        self._timer = None
        self._showCamera=False
        self._showFringes=False
        self._startShifting = False
        self._lock = threading.Lock()

    def start(self):
        self._th = threading.Thread(target = self._show, args = ())
        self._timer = threading.Timer(1.0, self._shift)
        self._th.start()

    def _show(self):
        if not self._capturer.isOpened():
            self.capturer.open(self.ncam)
        while True:
            if self._showCamera:
                self._show_camera()
            if self._showFringes:
                self._show_fringes()

            if cv2.waitKey(16) & 0xFF == ord('q'):
                self._showCamera = False
                cv2.destroyWindow('Camera')
            if cv2.waitKey(16) & 0xFF == ord('s'):
                self._showFringes = False
                cv2.destroyWindow('Camera')

            if self._close:
                self._showCamera = False
                self._showFringes = False
                cv2.destroyAllWindows()
                break

    def _show_camera(self):
        with self._lock:
            ret, frame = self._capturer.read()
            im = cv2.cvtColor(frame, cv2.COLOR_BGRA2GRAY)
        cv2.imshow('Camera', im)

    def _shift(self):
        self.capture_frame()
        self._fringes.next_strips()
        if self._startShifting:
            self._timer = threading.Timer(1.0, self._shift)
            self._timer.start()

    def startShifting(self):
        self._startShifting = True
        self._timer.start()

    def stopShifting(self):
        self._startShifting = False
        self._timer = threading.Timer(1.0, self._shift)

    def _show_fringes(self):
        I = self._fringes.get()
        cv2.imshow('Strips', I)

    def showFringes(self):
        self._showFringes = True

    def showCamera(self):
        self._showCamera = True

    def setFringes(self, fringes):
        self._fringes = fringes

    def capture_frame(self):

        if self._capturer.isOpened():
            with self._lock:
                ret, frame = self._capturer.read()
                im = cv2.cvtColor(frame, cv2.COLOR_BGRA2GRAY)

            self._frame_list.append(im)
        else:
            print("Oh oh!!... The camera is not opened")

    def save_images(self):
        n=0
        for im in self._frame_list:
            name = "%.3d.png" % n
            cv2.imwrite(name, im)
            n+= 1

    def get_frames(self):
        return self._frame_list

    def __del__(self):
        self._close = True
        self._th.join()
        print("Camera closed... Bye!!")