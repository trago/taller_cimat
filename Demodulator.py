import numpy as np
import scipy as sp

class Demodulator(object):
    def __init__(self, N = 256, M = 256, alpha = np.pi/2.0):
        self.N = N
        self.M = M
        self.alpha = alpha
        self.T = np.zeros(shape = (self.N, self.M), dtype = complex)

    def set_list(self, list):
        self.I = list
        self.phase = np.array((self.N, self.M))

        if len(self.I) == 5:
            self.T.real = (2.0*self.I[2] - self.I[0] - self.I[4])*np.sin(self.alpha)
            self.T.imag = 2.0*self.I[1] - 2.0*self.I[3] - (self.I[0] - self.I[4])*np.cos(self.alpha)

    def remove(self, fx, fy, sign = -1):
        wm = 2.0 * np.pi * fy
        wn = 2.0 * np.pi * fx

        #Jr = np.zeros((self.N, self.M))
        #Ji = np.zeros((self.N, self.M))


        if len(self.I) == 5:
            n,m = np.ogrid[0:self.N, 0:self.M]
            s=(self.T.real + self.T.imag*1.j)* np.exp(1.j * sign * (wm*m + wn*n))
            #J = np.arctan2(s.real, s.imag)
            #for n in range(0, self.N):
            #    for m in range(0, self.M):
            #        Jr[n, m] = self.T[n, m].real * np.cos(-wm * m) - self.T[n, m].imag * np.sin(-wm * m)
            #        Ji[n, m] = self.T[n, m].imag * np.cos(-wm * m) + self.T[n, m].real * np.sin(-wm * m)

        #self.T.real = Jr
        #self.T.imag = Ji
        self.T.real = s.real
        self.T.imag = s.imag

    def get_magnitude(self):
        return np.sqrt(self.T.real * self.T.real + self.T.imag * self.T.imag)

    def get_phase(self):
        return np.arctan2(self.T.real, self.T.imag)

    def get_k(self):
        F = sp.fft(np.cos(np.arctan2(self.T.real, self.T.imag)))

        cmax = -1
        nmax = -1
        mmax = -1

        for n in range(0, self.N):
            for m in range(0, self.M):
                norma = np.sqrt(F[n, m]*F[n, m].conjugate()).real
                if norma > cmax:
                    cmax = norma
                    nmax = n
                    mmax = m

        print(nmax)
        print(mmax)
        return (mmax, nmax)